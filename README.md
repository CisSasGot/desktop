# Gitter Desktop Client

[![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/gitterHQ/desktop?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

This is the desktop client for Linux, Windows, and macOS.

Windows | macOS | Linux (Ubuntu)
--- | --- | ---
![](https://i.imgur.com/pE3I2b2.png) | ![](https://i.imgur.com/Y15LCbV.png) | ![](https://i.imgur.com/a18qkax.png)

## Latest Builds

For the official downloads, please visit [https://gitter.im/apps](https://gitter.im/apps).


## Manually sign-in/authorize

If you don't want to type your GitLab/GitHub/Twitter credentials into the app, you can manually authorize with the following instructions,

 1. Open the desktop preferences file,
    - Windows: `C:\Users\<YOUR_USERNAME>\AppData\Local\Gitter\User Data\Default\gitter_preferences.json`
    - Linux: `/home/<YOUR_USERNAME>/.config/Gitter/Default/gitter_preferences.json`
    - macOS: `/Users/<YOUR_USERNAME>/Library/Application Support/Gitter/Default/gitter_preferences.json`
 1. Go to https://developer.gitter.im/apps, sign-in and copy your **Personal Access Token**
 1. In `gitter_preferences.json`, paste the token under the `preferences` -> `token` key,

```json
{
  "preferences": {
    "token": "xxx",
  }
}
```

You can track https://gitlab.com/gitlab-org/gitter/desktop/issues/216 for popping the OAuth flow out to a trusted browser


## Running The Development Version

The Gitter Desktop client is written using [NW.js](http://nwjs.io/), but the only prerequisite is [node.js](http://nodejs.org/download) for your platform and npm 3+.

 1. Clone this repo: `git clone git@gitlab.com:gitlab-org/gitter/desktop.git && cd desktop`
 1. Install all dependencies: `npm install`
 1. Generate your own [app oAuth credentials](https://developer.gitter.im/apps) where the redirect url is `https://gitter.im/login/desktop/callback`
 1. Start the app with your credentials:
    - macOS/Linux: `OAUTH_KEY=yourkey OAUTH_SECRET=yoursecret npm start`
    - Windows cmd: `set OAUTH_KEY=yourkey && set OAUTH_SECRET=yoursecret && npm start`
    - Alternatively, you can create an `oauth.json` file in the `nwapp/` folder. Place your keys and secrets in this file like this example, where `"osx"` represents your platform as defined [here](nwapp/utils/client-type.js): `{ "osx": { "KEY": "yourkey", "SECRET": "yoursecret" }}`. Once done, run `npm start`


### CLI parameters

 - `--log-level`: `[trace|debug|info|warn|error]` The level of logs to show (we are using [`loglevel`](https://www.npmjs.com/package/loglevel))
 - `--base-url`: Use the desktop app against a given Gitter instance. e.g. `--base-url=http://localhost:5000`
    - Don't forget to update your OAuth keys `nwapp/oauth.json` for a correct redirect URI
    - Also update `webapp/config/config.user-overrides.json` to point correctly (if using a local IP for example)
 - `--faye-url`: Use the desktop app against a given Gitter instance. e.g. `--faye-url=http://localhost:5000/bayeux`
 - `--update-url`: The base URL we use to check for `package.json` manifest updates and downloads. e.g. `--update-url=localhost:3010`

We use NW.js, so you can use any of those CLI parameters like [`--remote-debugging-port=port`](https://github.com/nwjs/nw.js/wiki/debugging-with-devtools)


## Tray Icon on Ubuntu

To see the Gitter tray icon run:

```
sudo apt-add-repository ppa:gurqn/systray-trusty
sudo apt-get update
sudo apt-get upgrade
```

More info [here](http://ubuntuforums.org/showthread.php?t=2217458).


## Enabling Logging on Windows

 1. Install [Sawbuck](https://github.com/google/sawbuck/releases). This tool will capture logs for all chrome-based applications.
 1. Add "Content Shell" to your list of Providers in Sawbuck by adding these registry entries to your machine (NOTE the optional Wow6432Node key for x64 machines):
    1. Find:  `HKLM\SOFTWARE\[Wow6432Node\]Google\Sawbuck\Providers`
    1. Add a subkey with the name `{6A3E50A4-7E15-4099-8413-EC94D8C2A4B6}`
    1. Add these values:
       - Key: `(Default)`, Type: `REG_SZ`, Value: `Content Shell`
       - Key: `default_flags`, Type: `REG_DWORD`, Value: `00000001`
       - Key: `default_level`, Type: `REG_DWORD`, Value `00000004`
       - Alternatively, use [this .reg file](http://cl.ly/1K0R2o1r1K0Z/download/enable-gitter-logging.reg) to do the above for you (in x86) (courtesy of @mydigitalself).
 1. Start Sawbuck and go to `Log -> Configure Providers` and change Content Shell's Log Level to `Verbose`. There are additional privacy-related changes that you may wish to make; see [Important Privacy and Security Notice](#important-privacy-and-security-notice), below.
 1. Start capturing logs by going to `Log -> Capture`.
 1. Start up your Gitter app and watch those logs!

#### Important Privacy and Security Notice

Sawback captures logging data from **all** running Chrome instances (not just the Gitter Desktop client), so its logs may include URLs you visited, search queries you executed, and the like.

To minimize the risk of including sensitive information in a publicly-posted logging session, you are advised to change the `Configure Providers` options such that the `Log Level` value is set to `None` for every Provider *except* for `Content Shell`. *Always* review logs for sensitive information and sanitize as appropriate before posting them publicly.


## Releasing the app (win32 and linux32/64)

### Prerequisites

All platforms, install/setup the following dependencies:

 - AWS CLI: https://aws.amazon.com/cli/
 - `aws configure` (get access key/secret key credentials from https://console.aws.amazon.com/iam/home#/users and create a "Access key")

On macOS/Linux, install the following dependencies:

 - `brew cask install java xquartz` then `brew install wine`
 - `brew install gnu-tar` then `sudo gem install fpm`
 - If on macOS(because of a bug with Yosemite), Open `/Library/Ruby/Gems/2.0.0/gems/ffi-1.9.10/lib/ffi/library.rb` and do the fix proposed here: https://github.com/ffi/ffi/issues/461#issuecomment-149253757

### Build

 1. Build all app artifacts
    1. On macOS, run `gulp cert:fetch:osx` (`certificates/DeveloperID.p12`)
    1. On macOS, run `gulp build:osx`
    1. On macOS/Linux, run `gulp build:linux`
    1. On macOS/Linux, run `gulp cert:fetch:win` (`certificates/troupe-cert.pfx`)
    1. On Windows, run `npm run build` -> `node ./windows/build.js -p thepfxcertpasswordhere`. For more information, see the [windows build readme](https://gitlab.com/gitlab-org/gitter/desktop/blob/master/windows/README.md)
    1. On any platform, create the redirect pages with `gulp redirect:source`
 1. **Check that all the binaries work**. You should have:
    - `GitterSetup-X.X.X.exe`
    - `Gitter-X.X.X.dmg`
    - `gitter_X.X.X_amd64.deb`
    - `gitter_X.X.X_i386.deb`
    - `latest_linux32.html`
    - `latest_linux64.html`
    - `latest_win.html`
 1. Publish the release (all uploads will throw a `formatError`!)
    1. On macOS/Linux, run `gulp artefacts:push:osx`
    1. On macOS/Linux, run `gulp artefacts:push:linux32`
    1. On macOS/Linux, run `gulp artefacts:push:linux64`
    1. On Windows, run `gulp artefacts:push:win`
    1. On the platform you created the redirect pages, run `gulp redirect:push:win`
    1. On the platform you created the redirect pages, run `gulp redirect:push:linux32`
    1. On the platform you created the redirect pages, run `gulp redirect:push:linux64`



# Contributing

Please see the [contribution guidelines](CONTRIBUTING.md)
